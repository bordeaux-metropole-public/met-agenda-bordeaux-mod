# Changelog
All notable changes to this project will be documented in this file.

## [1.3.0] - 2020-01-08
### Fixed
 - Conditionnement affichage du formulaire agenda dans l'ajout d'un node
 
## [1.0.0] - 2020-08-04
### Added
 - Initialisation du projet


# Templates:

## [x.x.x] - YYYY-MM-DD
### Added
### Changed
### Removed
### Deprecated
### Fixed
### Security
