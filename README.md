# MET-AGENDA-BORDEAUX-MOD

Module permettant de soumettre un événement à bordeaux.fr


# Configuration

Pour fonctionner, il faut rajouter aux taxonomies dites ciblées "type evenements" et "type public" respectivement un champ integer
avec en nom :

```
field_bam_type_evenement_id
field_bam_type_public_id
```




# Module Bordeaux Agenda Musée

## Settings :
{{url}}/admin/config/services/bordeaux-agenda/settings

## Node :
{{url}}/admin/config/services/bordeaux-agenda/node

## Entity :
{{url}}/admin/config/services/bordeaux-agenda/entity

## Logs :
{{url}}/admin/reports/bordeaux-agenda



