<?php

namespace Drupal\bordeaux_agenda_musee;

use Drupal;

trait TraitCommonForm
{
  protected $entity;

  protected $entityId;

  protected $bundleId;

  protected $configuration;

  protected $configurationName;

  protected $entityType;

  /**
   * @return mixed
   */
  public function getEntity()
  {
    return $this->entity;
  }

  /**
   * @param mixed $entity
   */
  public function setEntity($entity)
  {
    $this->entity = $entity;
  }

  /**
   * @return mixed
   */
  public function isEntityNode()
  {
    return $this->getEntityType() == 'node';
  }

  /**
   * @return mixed
   */
  public function getEntityType()
  {
    return $this->entityType;
  }

  /**
   * @param mixed $entityType
   */
  public function setEntityType($entityType)
  {
    $this->entityType = $entityType;
  }

  public function getEntityId() {
    return $this->entityId;
  }

  public function getBundleId() {
    return $this->bundleId;
  }

  protected function getConfiguration() {
    return $this->configuration;
  }

  protected function getConfigurationEntity() {
    return $this->getConfiguration()->get($this->getBundleId());
  }

  protected function getRouteEdit() {
    return 'bordeaux_agenda_musee.admin.configuration.edit';
  }

  protected function getRouteAdmin() {
    return $this->isEntityNode() ?  'bordeaux_agenda_musee.admin.configuration.node' : 'bordeaux_agenda_musee.admin.configuration.entity';
  }

  protected function getRouteDelete() {
    return 'bordeaux_agenda_musee.admin.configuration.delete';
  }

  protected function setConfigurationName() {

    if($this->isEntityNode()) {
      $this->configurationName = 'bordeaux_agenda_musee.node';
    }
    else
    {
      $this->configurationName = 'bordeaux_agenda_musee.entity';
    }
  }

  /**
   * @return mixed
   */
  public function getConfigurationName()
  {
    return $this->configurationName;
  }

  protected function setConfigurationFormState($form_state) {
    $entityType = $form_state->getformObject()->getEntity()->getEntityType()->id();
    $bundleId = $form_state->getformObject()->getEntity()->getEntityType()->id() == 'node' ?
      $form_state->getformObject()->getEntity()->getType() : $form_state->getformObject()->getEntity()->getEntityType()->id();

    $this->setConfiguration($entityType, $bundleId);
  }

  protected function setConfiguration($entityType = null, $bundleId = null) {
    $this->bundleId = $bundleId;
    $this->entityType = $entityType;
    $this->setConfigurationName();
    $this->configuration = Drupal::config($this->getConfigurationName());
    return $this->configuration;
  }
}
