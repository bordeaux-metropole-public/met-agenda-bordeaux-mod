<?php

namespace Drupal\bordeaux_agenda_musee\Service;

use Drupal;
use Drupal\bordeaux_agenda_musee\TraitCommonForm;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Cette Classe permet l'affichage des champs à renseigner pour l'envoi des données à Bordeaux Agenda.
 * Champs configurés dans : /admin/config/services/bordeaux-agenda/node
 *
 * Class AgendaFormService
 * @package Drupal\bordeaux_agenda_musee\Service
 */
class AgendaFormService {

  use TraitCommonForm;

  /**
   * @param $form
   * @param $form_state
   * @return mixed
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function getForm(&$form, $form_state) {

    $this->setConfigurationFormState($form_state);
    $siteName = Drupal::token()->replace('[site:name]');



    $form['bordeaux_agenda_musee'] = [
      '#tree' => TRUE,
      '#type' => 'details',
      '#title' => '<img class="bam-logoinform" src="/modules/custom/bordeauxagendamusee/img/logovillebordeauxmini.jpg"/> '.
        t('Transmission BORDEAUX.FR'),
      '#description'=> t("Utilisez ce formulaire pour transmettre cet événement à l'agenda Bordeaux.Fr"),
      '#weight' => 60,
      '#open'=>FALSE,
    ];

    $form['bordeaux_agenda_musee']['transmission'] = [
      '#type' => 'checkbox',
      '#title' => t('Transmettre à Bordeaux.Fr'),
      '#default_value' => False,
    ];

     $form['bordeaux_agenda_musee']['typologie'] = [
      '#type' => 'fieldset',
      '#title' => t('Typologie'),
      '#open'=>false,
    ];

    $typesEvenement = $this->getListTaxonomy('typeEvenement', 'field_bam_type_evenement_id');
    if($typesEvenement) {
      $form['bordeaux_agenda_musee']['typologie']['typeEvenement'] = [
        '#type' => 'select',
        '#title' => t('Type d\'événenemt'),
        '#options' => $typesEvenement
      ];
    }

    $typesPublic = $this->getListTaxonomy('typePublic', 'field_bam_type_public_id');
    if($typesPublic) {
      $form['bordeaux_agenda_musee']['typologie']['typePublic'] = [
        '#type' => 'select',
        '#title' => t('Type de Public'),
        '#options' => (['' => t('Tout public')] + $typesPublic)
      ];
    }

    $form['bordeaux_agenda_musee']['date'] = [
      '#type' => 'fieldset',
      '#title' => t('Date événement'),
    ];

    $form['bordeaux_agenda_musee']['date']['commentDate'] =
      $this->generateTextarea('commentDate', t('Précisions sur les dates et horaires'));

    $form['bordeaux_agenda_musee']['organisateur'] = [
      '#type' => 'fieldset',
      '#title' => t('Organisateurs'),
    ];

    $form['bordeaux_agenda_musee']['organisateur']['idOrganisateur'] = [
      '#type' => 'radios',
      '#default_value' => 'site',
      '#options' => [
        'site' => t('Le @organisateur est l\'organisateur', ['@organisateur' => $siteName]),
        'other' => t('Autre organisateur, précisez')
      ],
    ];

    // Création d'un conteneur pour gestion du conditionnement pour la textarea et son suffixe.
    $form['bordeaux_agenda_musee']['organisateur']['renseignement_orga_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [':input[name="bordeaux_agenda_musee[organisateur][idOrganisateur]"]' => ['value' => 'other'],],
      ],
    ];

    $form['bordeaux_agenda_musee']['organisateur']['renseignement_orga_container']['renseignementOrganis'] =
      $this->generateTextarea('renseignementOrganis', "");

    $form['bordeaux_agenda_musee']['evenement'] = [
      '#type' => 'fieldset',
      '#title' => t('Lieu de l\'événement'),
    ];

    $form['bordeaux_agenda_musee']['evenement']['idLieu'] = [
      '#type' => 'radios',
      '#default_value' => 'site',
      '#options' => [
        'site' => t('L\'événement se passe au @lieu', ['@lieu' => $siteName]),
        'other' => t('Autre lieu, précisez')
      ],
    ];

    // Création d'un conteneur pour gestion du conditionnement pour la textarea et son suffixe.
    $form['bordeaux_agenda_musee']['evenement']['commentaire_lieu_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [':input[name="bordeaux_agenda_musee[evenement][idLieu]"]' => ['value' => 'other'],],
      ],
    ];

    $form['bordeaux_agenda_musee']['evenement']['commentaire_lieu_container']['commentaireLieu'] =
      $this->generateTextarea('commentaireLieu', "");

    $form['bordeaux_agenda_musee']['evenement']['idHandi'] = [
      '#type' => 'checkbox',
      '#title' => t('Accès Handicapé'),
      '#default_value' => False,
    ];

    $form['bordeaux_agenda_musee']['tarif'] = [
      '#type' => 'fieldset',
      '#title' => t('Tarifs'),
    ];

    $form['bordeaux_agenda_musee']['tarif']['entreeLibre'] = [
      '#type' => 'checkbox',
      '#title' => t('Entrée libre'),
      '#default_value' => False,
    ];

    $form['bordeaux_agenda_musee']['tarif']['tarifs'] =
      $this->generateTextarea('tarifs', t('Tarifs si entrée payante'));

    $form['bordeaux_agenda_musee']['description'] = [
      '#type' => 'details',
      '#title' => t('Détails du post'),
      '#open' => FALSE,
    ];

    $form['bordeaux_agenda_musee']['description']['liensEnSavoirPlus'] = [
      '#type' => 'textfield',
      '#title' => t('Pour en savoir plus'),
      '#value' => "[node:url]",
    ];

    $form['bordeaux_agenda_musee']['description']['resume'] =
      $this->generateTextarea('resume', t('Texte court de la présentation'));
    $form['bordeaux_agenda_musee']['description']['descDetail'] =
      $this->generateTextarea('descDetail', t('Détail de l\'événement'));


    /* Parametres supplémentaire du post */
    $form['bordeaux_agenda_musee']['post'] = [
      '#type' => 'details',
      '#title' => t('Données du post'),
      '#open' => FALSE,
    ];

    $form['bordeaux_agenda_musee']['post']['titre'] = [
      '#type' => 'textfield',
      '#title' => t('Titre du post'),
      '#default_value' => $this->getConfigurationEntity()['fields']['titre'],
    ];

    $form['bordeaux_agenda_musee']['post']['vCourriel'] = [
      '#type' => 'textfield',
      '#title' => t('Adresse Email de l\'auteur du post'),
      '#default_value' => $this->getConfigurationEntity()['fields']['vCourriel'],
    ];

    $form['bordeaux_agenda_musee']['post']['vNom'] = [
      '#type' => 'textfield',
      '#title' => t('Nom de l\'auteur du post'),
      '#default_value' => $this->getConfigurationEntity()['fields']['vNom'],
    ];

    /* Parametres supplémentaire du post */
    $form['bordeaux_agenda_musee']['photo'] = [
      '#type' => 'details',
      '#title' => t('Illustration'),
      '#open' => FALSE,
    ];

    $form['bordeaux_agenda_musee']['photo']['active'] = [
      '#type' => 'checkbox',
      '#title' => t('Utilisation d\'une illustration'),
      '#default_value' => $this->getConfigurationEntity()['fields']['photo']['active'],
    ];

    $form['bordeaux_agenda_musee']['photo']['photojointe'] = [
      '#type' => 'textfield',
      '#title' => t('Url l\'image'),
      '#default_value' => $this->getConfigurationEntity()['fields']['photo']['photojointe'],
    ];

    $form['bordeaux_agenda_musee']['photo']['legendePhotoJointe'] = [
      '#type' => 'textfield',
      '#title' => t('Description de l\'image'),
      '#default_value' => $this->getConfigurationEntity()['fields']['photo']['legendePhotoJointe'],
    ];

    $form['bordeaux_agenda_musee']['photo']['copyrightPhotoJointe'] = [
      '#type' => 'textfield',
      '#title' => t('Copyright de l\'image'),
      '#default_value' => $this->getConfigurationEntity()['fields']['photo']['copyrightPhotoJointe'],
    ];

    return $form;
  }

  public function getListTaxonomy(string $taxonomyId, string $taxonomyKey) {
    $list = [];
    if($this->getConfigurationEntity()['fields'][$taxonomyId] == '') {
      return false;
    }
    $terms = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->getConfigurationEntity()['fields'][$taxonomyId]);
    foreach ($terms as $term) {
      $id = Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid)->get($taxonomyKey)->getValue()[0]['value'];
      $list[$id] = $term->name;
    }
    return $list;
  }

  /**
   * @param $key
   * @param $title
   * @return array
   */
  public function generateTextarea($key, $title) {
    return [
      '#type' => 'textarea',
      '#title' => $title,
      '#maxlength' => $this->getConfigurationEntity()['fields'][$key]['length'],
      '#suffix'     => $this->generateLegend($key),
      '#default_value' => $this->getConfigurationEntity()['fields'][$key]['value'],
    ];
  }

  /**
   * @param $key
   * @return string
   */
  public function generateLegend($key) {
    $legendBase = $this->getConfigurationEntity()['fields'][$key]['legend'];
    $legend =  '<div class="bam-textarea-legend">';
    if (!empty($legendBase)) {
      $legend .= $legendBase . '<br/>';
    }
    $legend .= t('Nombre maximum de caractères : ') . $this->getConfigurationEntity()['fields'][$key]['length'];
    $legend .= '</div>';
    return $legend;
  }
}
