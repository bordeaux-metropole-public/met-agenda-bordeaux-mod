<?php

namespace Drupal\bordeaux_agenda_musee\Service;


use Drupal;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaEntityStorage;
use Drupal\bordeaux_agenda_musee\TraitCommonForm;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\Config\Definition\Exception\Exception;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaStorage;

/**
 * Cette Classe permet la gestion de l'envoi des données vers Bordeaux Agenda,
 * suite à l'enregistrement d'un Node
 *
 * Class AgendaSubmitService
 * @package Drupal\bordeaux_agenda_musee\Service
 */
class AgendaSubmitService {

  /**
   * @var
   */
  private $email;

  /**
   * @var
   */
  private $directory;

  /**
   * @var
   */
  private $url;

  /**
   * @var
   */
  private $test;

  /**
   * @var
   */
  private $debug;

  /**
   * @var
   */
  private $purgeToken;

  /**
   * @var
   */
  private $decodeChar;

  /**
   * @var
   */
  private $placeId;

  /**
   * @var
   */
  private $organizerId;

  /**
   * @var
   */
  private $drupalToken;

  /**
   * @var
   */
  private $configSettings;

  /**
   * @var
   */
  private $configNode;

  /**
   * @var
   */
  private $idEntity;

  use TraitCommonForm;

  /**
   * Méthode global pour afficher et mettre en log des warning
   * @param $message
   */
  public function logNotice($message) {
    Drupal::logger('bordeaux_agenda_musee')->notice($message);
    Drupal::messenger()->addMessage($message);
  }

  /**
   * Méthode global pour afficher et mettre en log des erreurs
   * @param $message
   */
  public function logError($message) {
    Drupal::logger('bordeaux_agenda_musee')->error($message);
    Drupal::messenger()->addMessage($message);
  }

  /**
   * Méthode permettant d'encodé le texte pour la plateforme Bordeaux Agenda
   * @param $str
   * @return bool|string
   */
  protected function encodeToIso($str) {
    return iconv("UTF-8", "ISO-8859-1//TRANSLIT", $str);
  }

  /**
   * Get image path of $style_name style and check its size and extension.
   * If size or extension is not 'bordeaux.fr valid' a warning is adding and a message is displaying to the user.
   * @param $file_target_id string Original image file id.
   * @return string Image path with $style_name style, NULL if style not exist.
   */
  protected static function getOtherStyleImageRealPath($file_target_id) {
    $img_realpath = NULL;

    $file = File::load($file_target_id);
    if (!$file) {
      return NULL;
    }

    $image_file_uri = $file->getFileUri();

    /* Load image style to use and get it's image URI (default: bordeaux_agenda_musee_image style). */
    $style_name = Drupal::config('bordeaux_agenda_musee.settings')->get('bordeaux_agenda_musee_image_style');
    if (empty($style_name)) {
      $style_name = 'bordeaux_agenda_musee_image';
      \Drupal::logger('bordeaux_agenda_musee')
        ->info("Le style d'image n'est pas configuré, utilisation du style '@style'.", ['@style' => $style_name]);
    }
    $style = ImageStyle::load($style_name);
    if ($style != NULL) {
      $other_style_file_uri = $style->buildUri($image_file_uri);
      /* Create image with style to use if does not exist. */
      if (!file_exists($other_style_file_uri)) {
        $style->createDerivative($image_file_uri, $other_style_file_uri);
      }
      $img_realpath = \Drupal::service('file_system')->realpath($other_style_file_uri);
    } else {
      \Drupal::logger('bordeaux_agenda_musee')
        ->warning("Le style '@style' n'existe pas, non transmission de l'image.", ['@style' => $style_name]);
    }

    /* Size and extension checks. */
    if ($img_realpath != NULL) {
      $checks = TRUE;
      /* Size check. */
      $max_size = Drupal::config('bordeaux_agenda_musee.settings')->get('bordeaux_agenda_musee_image_max_size') ?: 1.5;
      if (filesize($img_realpath) > $max_size*pow(1024, 2)) {
        $checks = FALSE;
        \Drupal::logger('bordeaux_agenda_musee')
          ->warning("La taille du fichier de style '@style' est supérieure à @size Mo, non transmission de l'image.",
            ['@style' => $style_name, '@size' => $max_size]);
      }

      /* Extension check. */
      $authorized_extensions = Drupal::config('bordeaux_agenda_musee.settings')
        ->get('bordeaux_agenda_musee_image_authorized_extensions') ?: 'jpeg jpg gif png';
      $image_extension = pathinfo($img_realpath)['extension'];
      if (!in_array(strtolower($image_extension), explode(' ', $authorized_extensions))) {
        $checks = FALSE;
        \Drupal::logger('bordeaux_agenda_musee')
          ->warning("L'extention '@extension' n'est pas autorisée (attendues : @extensions), non transmission de l'image.",
            ['@extension' => $image_extension, '@extensions' => $authorized_extensions]);
      }
      if (!$checks) {
        $img_realpath = NULL;
      }
    }

    /* Add message for user if no image will be sent. */
    if ($img_realpath == NULL) {
      \Drupal::messenger()
        ->addMessage("Transmission de l'image à bordeaux.fr impossible. Contactez votre responsable applicatif pour plus d'information.");
    }

    return $img_realpath;
  }

  /**
   * @param $form_state
   * @return mixed
   */
  private function getIdEntity($form_state) {
    return $form_state->getformObject()->getEntity()->getEntityType()->id() == 'node' ?
      $form_state->getformObject()->getEntity()->getType() : $form_state->getformObject()->getEntity()->getEntityType()->id();
  }

  /**
   * Méthode "globale" permettant le traitement des données issues du node et l'envoi ou non (TEST/DEBUG)
   * @param $form_state
   * @return bool|void
   */
  public function sendData($form_state) {
    $this->idEntity = $form_state->getformObject()->getEntity()->getEntityType()->id();
    $this->bundleId = $this->getIdEntity($form_state);
    $this->entity = $form_state->getformObject()->getEntity();
    $this->setConfiguration($form_state->getformObject()->getEntity()->getEntityType()->id(), $this->getIdEntity($form_state));
    $this->drupalToken = Drupal::token();
    $this->configSettings = Drupal::config('bordeaux_agenda_musee.settings');
    $this->directory = $this->configSettings->get('bordeaux_agenda_musee_debug_directory');
    $this->email = $this->configSettings->get('bordeaux_agenda_musee_destination_mail');
    $this->url = $this->configSettings->get('bordeaux_agenda_musee_post_url');
    $this->placeId = $this->configSettings->get('bordeaux_agenda_musee_place_id');
    $this->organizerId = $this->configSettings->get('bordeaux_agenda_musee_organizer_id');
    $this->configNode = $this->configuration->get($this->getBundleId());
    $this->test = $this->configNode['configuration']['test'];
    $this->debug = $this->configNode['configuration']['debug'];
    $this->purgeToken = isset($this->configNode['configuration']['purgetoken']) ? $this->configNode['configuration']['purgetoken']:false;
    $this->decodeChar = isset($this->configNode['configuration']['decodechar']) ? $this->configNode['configuration']['decodechar']:false;

    $bordeaux_agenda_musee = $form_state->getValue('bordeaux_agenda_musee');
    $notificationmail = $form_state->getValue('notificationmail');

    /* Si l'événement a déjà été transmis, on envoie juste un mél,
     * sinon si la transmission n'est pas souhaitée, on ne fait rien,
     * sinon on envoie l'événement. */
    $nidStorage = BordeauxAgendaStorage::exists($this->entity->id(), $this->getEntityType());
    if ($nidStorage) {
      if($notificationmail) {
        $this->logNotice("Envoi d'une notification au service communication");
        return $this->sendEmail();
      }
      return true;
    } else if(!$bordeaux_agenda_musee['transmission']) {
      return true;
    }

    $data = $this->prepareData($form_state->getValue('bordeaux_agenda_musee'));

    $post_id = $this->postData($data, $this->entity->id());
    if (!$post_id) {
      return true;
    }

    if ($this->test) { //mode test
      BordeauxAgendaStorage::insert($this->getEntityType(), $this->bundleId, $this->entity->id(), $post_id, Drupal::time()->getRequestTime(), 1);
      $this->logNotice("MODE TEST : Ce contenu a été simulé transmis à l'agenda Bordeaux.fr");
    } else {
      BordeauxAgendaStorage::insert($this->getEntityType(), $this->bundleId, $this->entity->id(), $post_id, Drupal::time()->getRequestTime());
      $this->logNotice("Ce contenu a été transmis à l'agenda Bordeaux.fr");
    }
  }

  /**
   * Répartition du traitement des champs par fonctionnalitées
   * @param $values
   * @return mixed
   */
  public function prepareData($values) {
    $data['typeEvenement'] = $values['typologie']['typeEvenement'];
    $data['typePublic'] = $values['typologie']['typePublic'];
    $data = $this->prepareDate($data, $values['date']);
    $data = $this->prepareOrganisateur($data, $values['organisateur']);
    $data = $this->prepareEvenement($data, $values['evenement']);
    $data = $this->prepareTarif($data, $values['tarif']);
    $data = $this->prepareDescription($data, $values['description']);
    $data = $this->preparePhoto($data, $values['photo']);
    $data = $this->preparePost($data, $values['post']);
    $data = $this->prepareTechnicalData($data);
    return $this->traitementPostData($data);
  }

  /**
   * retrait des tokens non utilisé suite au passage du token
   * Décodage des quotes simple et double
   */
  protected function traitementPostData($data) {
    $entity_type_manager = Drupal::service('entity_type.manager');
    $entities = array_keys($entity_type_manager->getDefinitions());
    foreach ($data as $key => $value) {
      if (is_string($value)) { // CURLFile handling
        foreach($entities as $entity) {
          $value = preg_replace("/(\[".$entity."(:[[:alnum:]]+)*\])/i", '', $value);
        }
        $data[$key] = html_entity_decode($value, ENT_QUOTES | ENT_XML1, 'UTF-8');
      }
    }
    return $data;
  }

  /**
   * Conversion du champ Date pour être intégré à Bordeaux Agenda
   * @param $value
   * @return string
   */
  public function convertDatetime($value = null) {
    if(!$value) {
      return null;
    }
    if (is_numeric($value)) {
      $date_time = DrupalDateTime::createFromTimestamp($value);
    } else {
      $date_time = new DrupalDateTime($value, 'UTC');
    }
    return $date_time->format('d/m/Y');
  }

  /**
   * Méthode pour remplacer les les éléments textes de token
   * @param $value
   * @return mixed
   */
  public function replaceElementNode($value) {
    $drupal_token = $this->drupalToken->replace($value, [$this->idEntity => $this->entity], ['clear'=>true]);
    return preg_replace("/(\[".$this->idEntity."(:[[:alnum:]]+)*\])/i", '', $drupal_token);
  }

  /**
   * Méthode pour retirer les caractères html du texte
   * @param $value
   * @return mixed
   */
  public function stripTags($value) {
    return strip_tags($value);
  }

  /**
   * @param $value
   * @return mixed
   */
  public function traitementText($value) {
    return $this->stripTags($this->replaceElementNode($value));
  }

  /**
   * @param $value
   * @return mixed
   */
  public function traitementBloc($value, $config) {

    $text = substr($this->replaceElementNode($value), 0, $config['length']);

    if(!$config['filter_html']) {
      $text = ($this->stripTags($text));
    }
    // Suppression des lignes vides.
    $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", PHP_EOL, $text);
    // Permet de supprimer les retours de ligne en trop à la fin de la chaîne.
    $text = trim($text);
    return $text;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function prepareDate(&$data, $values) {
    $data['commentDate'] = $this->traitementBloc($values['commentDate'], $this->configNode['fields']['commentDate']);

    $dateDebut = $this->convertDatetime($this->replaceElementNode($this->configNode['fields']['dateDebut']));
    $dateFin = $this->convertDatetime($this->replaceElementNode($this->configNode['fields']['dateFin']));

    if ($dateFin && $dateDebut !== $dateFin) {
      $data['dateDebut'] = $dateDebut;
      $data['dateFin'] = $dateFin;
      return $data;
    }

    $data['date'] = $dateDebut;
    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function prepareOrganisateur(&$data, $values) {
    if($values['idOrganisateur'] == 'site') {
      $data['idOrganisateur'] = $this->organizerId;
    } else {
      $data['renseignementOrganis'] = $this->traitementBloc(
          $values['renseignement_orga_container']['renseignementOrganis'],
          $this->configNode['fields']['renseignementOrganis']);
    }
    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function prepareEvenement(&$data, $values) {
    if($values['idLieu'] == 'site') {
      $data['idLieu'] = $this->placeId;
    } else {
      $data['commentaireLieu'] = $this->traitementBloc(
        $values['commentaire_lieu_container']['commentaireLieu'],
        $this->configNode['fields']['commentaireLieu']);
    }
    $data['handi'] = $values['idHandi'] ? 1 : 2;
    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function prepareTarif(&$data, $values) {
    $data['entreeLibre'] = $values['entreeLibre'] ? 1 : 2;
    $data['tarifs'] = $this->traitementBloc($values['tarifs'], $this->configNode['fields']['tarifs']);
    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function prepareDescription(&$data, $values) {
    $data['liensEnSavoirPlus'] = $this->replaceElementNode($values['liensEnSavoirPlus']);

    if ($values['resume']) {
      $data['resume'] = $this->traitementBloc($values['resume'], $this->configNode['fields']['resume']);
    }
    else {
      $data['resume'] = "Non renseigne";
    }

    if ($values['descDetail']) {
      $data['descDetail'] = $this->traitementBloc($values['descDetail'], $this->configNode['fields']['descDetail']);
    }
    else {
      $data['descDetail'] = "Non renseigne";
    }

    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function preparePhoto(&$data, $values) {
    /* Préparation si transmission illustration cochée et si photo renseignée. */
    if (!$values['active'] || empty($this->replaceElementNode($values['photojointe']))) {
      return $data;
    }

    $photo_path = self::getOtherStyleImageRealPath($this->replaceElementNode($values['photojointe']));

    if ($photo_path==NULL) {
      return $data;
    }

    $file = Drupal::service('file_system')->realpath($photo_path);
    $mime = mime_content_type($file);
    $info = pathinfo($file);
    $name = $info['basename'];
    $data['photojointe'] = curl_file_create($file,$mime,$name);
    $data['legendePhotoJointe'] = $this->replaceElementNode($values['legendePhotoJointe']);
    $data['copyrightPhotoJointe'] = $this->replaceElementNode($values['copyrightPhotoJointe']);
    return $data;
  }

  /**
   * @param $data
   * @param $values
   * @return mixed
   */
  public function preparePost(&$data, $values) {
    $data['titre'] = $this->configSettings->get('bordeaux_agenda_musee_tag') . $this->traitementText($values['titre']);
    $data['vCourriel'] = $this->traitementText($values['vCourriel']);
    $data['vNom'] = $this->traitementText($values['vNom']);
    return $data;
  }

  /**
   * @param $data
   * @return mixed
   */
  public function prepareTechnicalData(&$data) {
    $data['isOkVerif'] = 'OK';
    $data['identifiant'] = 'portlets.common.formEvt.validate';
    return $data;
  }

  /**
   * @param $data
   * @param $nid
   * @return bool|int
   */
  public function postData($data, $nid) {
    $return = FALSE;
    $id = false;
    // mode test ?
    if ($this->test) { //mode test
      return $this->postTestData($data);
    }

    try {
      $error_message = "";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $this->url);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      // Return the full header only
      curl_setopt($ch, CURLOPT_HEADER, 1);
      // User agent: without this, it seems it fails
      curl_setopt ($ch, CURLOPT_USERAGENT, "Drupal 8 - Bordeaux Metropole");
      $return = curl_exec($ch);

      if ($return === false) {
        $error_message = curl_error($ch);
      } else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
        $error_message = "Code retour: " . curl_getinfo($ch, CURLINFO_HTTP_CODE);
      } else {
        // The ID of the submission is given in the header:
        // HTTP/1.1 200 XXXXX
        // So we must parse all the header in order to find it.
        $lines = preg_split('/\r\n|\n|\r/', $return);
        $pattern = 'HTTP/1.1 200 ';
        foreach($lines as $line) {
          if (strpos($line, $pattern) === 0) {
            $id = intval(substr($line, strlen($pattern)));
            break;
          }
        }
        if ($id == false) {
          $error_message = "Impossible d'extraire l'id de soumission, d'apres la réponse: " . $return;
        }
      }

      curl_close($ch);

      if ($id) {
        Drupal::logger('bordeaux_agenda_musee')
          ->notice("Le post de l'agenda sur le portail bordeaux.fr pour le node @nid a retourne: @id", array(
            '@nid' => $nid,
            '@id' => $id
          ));
        Drupal::messenger()->addMessage("Evenement envoyé sur Bordeaux.fr avec l'identifiant " . $id);
      }
      else {
        Drupal::logger('bordeaux_agenda_musee')
          ->error("Le post de l'agenda sur le portail bordeaux.fr pour le node @nid a retourne: @return", array(
            '@nid' => $nid,
            '@return' => var_export($return, TRUE)
          ));
        Drupal::messenger()->addMessage("Un probleme est survenu lors du post de l'agenda sur le portail bordeaux.fr : " . var_export($return, TRUE), 'error');
      }

    } catch (Exception $e) {
      $debug['exception'] = $e->getMessage();
      Drupal::logger('bordeaux_agenda_musee')
        ->error("Un probleme de connexion est survenu lors du post de l'agenda sur le portail bordeaux.fr, cela concerne le node @nid", array('@nid' => $nid));
      Drupal::messenger()->addMessage("Un probleme de connexion est survenu lors du post de l'agenda sur le portail bordeaux.fr", 'error');
    }

    //debug ?
    if ($this->debug) { //mode test
      if (!file_exists($this->directory) || !is_writable($this->directory)) {
        Drupal::logger('bordeaux_agenda_musee')
          ->error("Le répertoire de destination du fichier de debug n'est pas accessible : @directory", array('@directory' => $this->directory));
        Drupal::messenger()->addMessage("Le répertoire de destination du fichier de debug n'est pas accessible.", 'error');
        return FALSE;
      }

      $file = fopen($this->directory . 'debug-agenda.log', 'a');
      fprintf($file, "=========================\n%s\n=========================\n", date("d/m/Y - H:i:s"));
      fprintf($file, "URL: %s\n\n", $this->url);
      fprintf($file, "DATA:\n%s\n\n", var_export($data, TRUE));
      fprintf($file, "RESPONSE:\n%s\n\n", ($return ? var_export($return, TRUE) : "pas de reponse, la requete a echoue"));
      if ($error_message) {
        fprintf($file, "RESPONSE ERROR:\n%s\n\n", $error_message);
      }
      fprintf($file, "\n");

      if ($file) {
        fclose($file);
      }
    }

    return $id;
  }

  /**
   * @param array $data
   * @return bool|int
   */
  public function postTestData($data = []) {
    if (!file_exists($this->directory) || !is_writable($this->directory)) {
      Drupal::logger('bordeaux_agenda_musee')
        ->error("Le répertoire de destination du fichier de test n'est pas accessible : @directory", array('@directory' => $this->directory));
      Drupal::messenger()->addMessage("Le répertoire de destination du fichier de test n'est pas accessible.", 'error');
      return FALSE;
    }
    $file = fopen($this->directory . 'test-agenda.log', 'a');
    fprintf($file, "=========================\n%s\n=========================\n", date("d/m/Y - H:i:s"));
    fprintf($file, "DATA:\n%s\n\n", var_export($data, TRUE));
    fprintf($file, "\n");
    if ($file) {
      fclose($file);
    }
    return Drupal::time()->getRequestTime();
  }

  /**
   *
   */
  public function sendEmail() {
    $user = Drupal::currentUser();
    $params = array(
      'user' => $user,
      'entity' => $this->entity,
      'entity_type' => $this->getEntityType(),
      'bundle_id' => $this->getBundleId()
    );
    $mailto = $this->email;
    $mailfrom = $user->getEmail();
    $langcode = $user->getPreferredLangcode();
    $mailManager = Drupal::service('plugin.manager.mail');
    $send = TRUE;

    $result = $mailManager->mail('bordeaux_agenda_musee', 'bordeaux_agenda_musee.request.notice', $mailto, $langcode, $params, $mailfrom, $send);

    if($result['result']) {
      $this->logNotice("Mail envoyé");
    }
    else {
      $this->logError("Erreur - Mail non envoyé");
    }

    return true;
  }
}
