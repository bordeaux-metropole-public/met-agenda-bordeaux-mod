<?php

namespace Drupal\bordeaux_agenda_musee\Form;

use Drupal;
use Drupal\bordeaux_agenda_musee\TraitCommonForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Url;

/**
 * Formulaire permettant la configuration d'un node
 *
 * Class AgendaNodeEditForm
 * @package Drupal\bordeaux_agenda_musee\Form
 */
class AgendaConfigurationEditForm extends ConfigFormBase {

  use TraitCommonForm;

  /**
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'), $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bordeaux_agenda_musee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'agenda.type.field';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_id = null, $bundle_id = null) {
    $config = $this->setConfiguration($entity_id, $bundle_id);
    $contenu = $config->get($bundle_id);
    if($contenu) {
      $data = $contenu;
    }
    else
    {
      $data = $this->initializeData();
      if($entity_id == 'node') {
        $data['fields']['dateDebut']['value'] = '[node:NOM_MACHINE:value]';
        $data['fields']['dateFin']['value'] = '[node:NOM_MACHINE:value]';
        $data['fields']['titre'] = '[node:title]';
        $data['fields']['vCourriel'] = '[node:author:mail]';
        $data['fields']['liensEnSavoirPlus'] = '[node:url]';
        $data['fields']['photojointe'] = '[node:NOM_MACHINE:0:entity:path]';
        $data['fields']['legendePhotoJointe'] = '[node:NOM_MACHINE:0:alt]';
        $data['fields']['copyrightPhotoJointe'] = '[node:NOM_MACHINE:0:entity:owner:name]';
      }
    }

    $form['fieldset_configuration'] = [
      '#type' => 'fieldset',
      '#title' => 'Configuration du Node',
    ];

    $form['fieldset_configuration']['active'] = [
      '#type' => 'checkbox',
      '#title' => 'Activé',
      '#default_value' => $data['configuration']['active']
    ];

    $form['fieldset_configuration']['debug'] = [
      '#type' => 'checkbox',
      '#title' => 'Débug',
      '#default_value' => $data['configuration']['debug']
    ];

    $form['fieldset_configuration']['test'] = [
      '#type' => 'checkbox',
      '#title' => 'Test',
      '#default_value' => $data['configuration']['test']
    ];

    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      $vocabulariesList[$vid] = $vocablary->get('name');
    }

    $form['fieldset_field'] = [
      '#type' => 'fieldset',
      '#title' => 'Configuration du Node',
    ];

    $form['fieldset_field']['typeEvenement'] = [
      '#type' => 'select',
      '#title' => 'Type d\'événement : ',
      '#options' => $vocabulariesList,
      '#default_value' => $data['fields']['typeEvenement'] ? $data['fields']['typeEvenement']:"bdx_type_evenement",
      '#required' => TRUE
    ];

    $form['fieldset_field']['typePublic'] = [
      '#type' => 'select',
      '#title' => 'Type de public : ',
      '#options' => $vocabulariesList,
      '#default_value' => $data['fields']['typePublic'] ? $data['fields']['typePublic']:"bdx_type_public",
    ];

    $form['fieldset_field']['dateDebut'] = [
      '#type' => 'textfield',
      '#title' => 'Date Début événement ',
      '#default_value' => $data['fields']['dateDebut'],
      '#required' => TRUE
    ];

    $form['fieldset_field']['dateFin'] = [
      '#type' => 'textfield',
      '#title' => 'Date Fin événement ',
      '#default_value' => $data['fields']['dateFin'],
      '#required' => TRUE
    ];

    $this->generateFieldBloc($form, $data, 'commentDate', 'Bloc Date');
    $this->generateFieldBloc($form, $data, 'commentaireLieu', 'Bloc Lieu');
    $this->generateFieldBloc($form, $data, 'tarifs', 'Bloc Tarifs');
    $this->generateFieldBloc($form, $data, 'renseignementOrganis', 'Bloc Organisateur');
    $this->generateFieldBloc($form, $data, 'resume', 'Bloc Résumé');
    $this->generateFieldBloc($form, $data, 'descDetail', 'Bloc Détail');


    /* Parametres supplémentaire du post */
    $form['bordeaux_agenda_musee']['photo'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => 'Illustration',
    ];

    $form['bordeaux_agenda_musee']['photo']['active'] = [
      '#type' => 'checkbox',
      '#title' => 'Utilisation d\'une illustration',
      '#default_value' => $data['fields']['photo']['active'],
    ];

    $form['bordeaux_agenda_musee']['photo']['photojointe'] = [
      '#type' => 'textfield',
      '#title' => 'Url l\'image :',
      '#default_value' => $data['fields']['photo']['photojointe'],
    ];

    $form['bordeaux_agenda_musee']['photo']['legendePhotoJointe'] = [
      '#type' => 'textfield',
      '#title' => 'Description de l\'image :',
      '#default_value' => $data['fields']['photo']['legendePhotoJointe'],
    ];

    $form['bordeaux_agenda_musee']['photo']['copyrightPhotoJointe'] = [
      '#type' => 'textfield',
      '#title' => 'Copyright de l\'image :',
      '#default_value' => $data['fields']['photo']['copyrightPhotoJointe'],
    ];

    $form['fieldset_field']['titre'] = [
      '#type' => 'textfield',
      '#title' => 'Champ titre : ',
      '#default_value' => $data['fields']['titre'],
      '#required' => TRUE
    ];

    $form['fieldset_field']['vCourriel'] = [
      '#type' => 'textfield',
      '#title' => 'Champ Courriel de l\'auteur : ',
      '#default_value' => $data['fields']['vCourriel'],
      '#required' => TRUE
    ];

    $form['fieldset_field']['vNom'] = [
      '#type' => 'textfield',
      '#title' => 'Champ Nom de l\'auteur : ',
      '#default_value' => $data['fields']['vNom'],
      '#required' => TRUE
    ];

    $form['fieldset_field']['liensEnSavoirPlus'] = [
      '#type' => 'textfield',
      '#title' => 'Lien en savoir Plus : ',
      '#default_value' => $data['fields']['liensEnSavoirPlus'],
      '#required' => TRUE
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 10,
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => 'Sauvegarder les modifications',
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#name' => 'cancel',
      '#value' => 'Annuler',
    ];

    $container = Drupal::getContainer();
    /** @var EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $entity_type_definition = $entity_type_manager->getDefinition($this->getEntityType());
    $entity_type_class = $entity_type_definition->getClass();
    $fields = call_user_func([$entity_type_class, 'baseFieldDefinitions'], $entity_type_definition);
    foreach ($fields as $key => $field_definition) {
      $tokens[$key]['label']  = is_string($field_definition->getLabel()) ? $field_definition->getLabel() : $key;
      $tokens[$key]['key'] = '['.$this->getEntityType().':' . $key . ']';
    }

    $form['fieldset_tokens'] = [
      '#type' => 'fieldset',
      '#title' => 'Liste des tokens possibles',
      '#weight' => 20,
    ];

    $form['fieldset_tokens']['tokens'] = [
      '#type' => 'table',
      '#header' => ['label', 'key'],
      '#rows' => $tokens,
      '#empty' => 'Aucun token',
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#name'] == 'save') {
      $configuration = [
        'active' => $form_state->getValue('active'),
        'debug' => $form_state->getValue('debug'),
        'test' => $form_state->getValue('test'),
      ];
      $fields = [
        'typeEvenement' => $form_state->getValue('typeEvenement'),
        'typePublic' => $form_state->getValue('typePublic'),
        'dateDebut' => $form_state->getValue('dateDebut'),
        'dateFin' => $form_state->getValue('dateFin'),
        'commentDate' => $form_state->getValue('commentDate'),
        'commentaireLieu' => $form_state->getValue('commentaireLieu'),
        'tarifs' => $form_state->getValue('tarifs'),
        'renseignementOrganis' => $form_state->getValue('renseignementOrganis'),
        'resume' => $form_state->getValue('resume'),
        'descDetail' => $form_state->getValue('descDetail'),
        'titre' => $form_state->getValue('titre'),
        'vCourriel' => $form_state->getValue('vCourriel'),
        'vNom' => $form_state->getValue('vNom'),
        'photo' => $form_state->getValue('photo'),
        'liensEnSavoirPlus' => $form_state->getValue('liensEnSavoirPlus'),
      ];

      $config = Drupal::service('config.factory')->getEditable($this->configurationName);
      $config->set($this->bundleId, ['configuration' => $configuration, 'fields' => $fields])->save();
    }
    $form_state->setRedirectUrl(Url::fromRoute($this->getRouteAdmin()));
  }

  /**
   * @return array
   */
  public function initializeData() {
    return [
      'configuration' => [
        'active' => true,
        'debug' => false,
        'test' => false,
      ],
      'fields' => [
        'typeEvenement' => '',
        'typePublic' => '',
        'dateDebut' => [
          'value' => '',
        ],
        'dateFin' => [
          'value' => '',
        ],
        'commentDate' => [
          'length' => 350,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'commentaireLieu' => [
          'length' => 350,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'tarifs' => [
          'length' => 350,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'renseignementOrganis' => [
          'length' => 350,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'resume' => [
          'length' => 300,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'descDetail' => [
          'length' => 1500,
          'legend' => '',
          'value' => '',
          'filter_html' => false,
        ],
        'titre' => '',
        'vCourriel' => '',
        'vNom' => '',
        'liensEnSavoirPlus' => '',
        'photo' => [
          'active' => false,
          'photojointe' => '',
          'legendePhotoJointe' => '',
          'copyrightPhotoJointe' => '',
        ],
      ],
    ];
  }

  /**
   * @param $form
   * @param $data
   * @param $key
   * @param $title
   * @return mixed
   */
  public function generateFieldBloc(&$form, $data, $key, $title) {

    $form['fieldset_field'][$key] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $title,
    ];

    $form['fieldset_field'][$key]['length'] = [
      '#type' => 'textfield',
      '#title' => 'Longueur max du bloc : ',
      '#default_value' => $data['fields'][$key]['length'],
      '#required' => TRUE
    ];

    $form['fieldset_field'][$key]['legend'] = [
      '#type' => 'textfield',
      '#title' => 'Légende du bloc : ',
      '#default_value' => $data['fields'][$key]['legend'],
    ];

    $form['fieldset_field'][$key]['value'] = [
      '#type' => 'textfield',
      '#title' => 'Valeur par défaut du bloc : ',
      '#default_value' => $data['fields'][$key]['value'],
    ];

    $form['fieldset_field'][$key]['filter_html'] = [
      '#type' => 'checkbox',
      '#title' => 'Texte riche avec balises html (case cochée)',
      '#default_value' => $data['fields'][$key]['filter_html'],
    ];

    return $form;
  }
}
