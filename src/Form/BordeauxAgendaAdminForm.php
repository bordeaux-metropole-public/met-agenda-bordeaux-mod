<?php

namespace Drupal\bordeaux_agenda_musee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\bordeaux_agenda\BordeauxAgendaStorage;
use Drupal\Core\Url;
use Drupal\Core\Link;
/**
 * Formulaire permettant la configuration globale du site : /admin/config/services/bordeaux-agenda/settings
 *
 * Class BordeauxAgendaAdminForm
 * @package Drupal\bordeaux_agenda_musee\Form
 */
class BordeauxAgendaAdminForm extends ConfigFormBase {

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'), $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bordeaux_agenda_musee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('bordeaux_agenda_musee.settings');

    $form['bordeaux_agenda_musee_post_url'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_post_url') ?:"http://bordeaux-rec/ebxCmPortlet/controlFormEvtServlet",
      '#title' => t('URL'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['bordeaux_agenda_musee_place_id'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_place_id'),
      '#title' => t('Place ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['bordeaux_agenda_musee_organizer_id'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_organizer_id'),
      '#title' => t('Organizer ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['bordeaux_agenda_musee_tag'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_tag'),
      '#title' => t('Tag'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['bordeaux_agenda_musee_debug_directory'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_debug_directory'),
      '#title' => t('Debug and Test directory'),
      '#required' => TRUE,
      '#type' => 'textfield',
      '#description' => 'ex: sites/madd/files/bordeaux_agenda_musee/'
    );

    $form['bordeaux_agenda_musee_destination_mail'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_destination_mail'),
      '#title' => t('Mail Destination'),
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['bordeaux_agenda_musee_image_style'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_image_style') ?: "bordeaux_agenda_musee_image" ,
      '#title' => t('Image style to use'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => 'ex: bordeaux_agenda_musee_image'
    );
    $form['bordeaux_agenda_musee_image_max_size'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_image_max_size') ?: '1.5',
      '#step' => 'any',
      '#title' => t('Maximum authorized image size (Mb)'),
      '#type' => 'number',
      '#required' => TRUE,
      '#description' => 'ex: 1.5'
    );
    $form['bordeaux_agenda_musee_image_authorized_extensions'] = array(
      '#default_value' => $config->get('bordeaux_agenda_musee_image_authorized_extensions') ?: 'jpeg jpg gif png',
      '#title' => t('Authorized image extensions'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => 'ex: jpeg jpg gif png'
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('bordeaux_agenda_musee.settings');
    $config->clear('bordeaux_agenda_musee.settings');
    if ($form_state->hasValue('bordeaux_agenda_musee_post_url')) {
      $config->set('bordeaux_agenda_musee_post_url', $form_state->getValue('bordeaux_agenda_musee_post_url'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_place_id')) {
      $config->set('bordeaux_agenda_musee_place_id', $form_state->getValue('bordeaux_agenda_musee_place_id'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_organizer_id')) {
      $config->set('bordeaux_agenda_musee_organizer_id', $form_state->getValue('bordeaux_agenda_musee_organizer_id'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_tag')) {
      $config->set('bordeaux_agenda_musee_tag', $form_state->getValue('bordeaux_agenda_musee_tag'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_debug_directory')) {
      $config->set('bordeaux_agenda_musee_debug_directory', $form_state->getValue('bordeaux_agenda_musee_debug_directory'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_destination_mail')) {
      $config->set('bordeaux_agenda_musee_destination_mail', $form_state->getValue('bordeaux_agenda_musee_destination_mail'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_image_style')) {
      $config->set('bordeaux_agenda_musee_image_style', $form_state->getValue('bordeaux_agenda_musee_image_style'));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_image_max_size')) {
      $config->set('bordeaux_agenda_musee_image_max_size', floatval($form_state->getValue('bordeaux_agenda_musee_image_max_size')));
    }
    if ($form_state->hasValue('bordeaux_agenda_musee_image_authorized_extensions')) {
      $config->set('bordeaux_agenda_musee_image_authorized_extensions',
        strtolower($form_state->getValue('bordeaux_agenda_musee_image_authorized_extensions')));
    }
    $config->save();
  }
}
