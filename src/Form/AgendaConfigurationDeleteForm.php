<?php

namespace Drupal\bordeaux_agenda_musee\Form;

use Drupal;
use Drupal\bordeaux_agenda_musee\TraitCommonForm;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Url;


/**
 * Class AgendaNodeDeleteSettingsForm
 * @package Drupal\bordeaux_agenda_musee\Form
 */
class AgendaConfigurationDeleteForm extends ConfigFormBase {

  use TraitCommonForm;

  /**
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'), $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bordeaux_agenda_musee.configuration.delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_id = null, $bundle_id = null) {

    $this->setConfiguration($entity_id, $bundle_id);
    $form['question'] = [
      '#markup' => t('Êtes-vous sûr de vouloir supprimer la configuration de l\'élément <i>@element</i> ?',
        ['@element' => $this->getBundleId(),]),
    ];

    $form['actions'] = ['#type' => 'actions',];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => t('Supprimer'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#name' => 'cancel',
      '#value' => t('Annuler'),
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#name'] == 'delete') {
      $config = Drupal::service('config.factory')->getEditable($this->getConfigurationName());
      $config->clear($this->getBundleId());
      $config->save();
    }
    $form_state->setRedirectUrl(Url::fromRoute($this->getRouteAdmin()));
  }
}
